url = "/"
action = "post"
VideoUrl = ''
ext = ""
$('#form-url-input').on('beforeSubmit', function() {
    $('#elem-container').empty()
    $('#elem-container img').src = ''
    var form = $(this);
    VideoUrl = this["YouTubeDownloader[video_url]"].value
    var data = form.serialize();
    // отправляем данные на сервер
    // console.log(data)
    $.ajax({
            url: url,
            type: action,
            data: data,
            beforeSend: function() {
                $('#loader').show();
            },

        })
        .done(function(data) {
            $('#loader').hide();
            data = JSON.parse(data)
            console.log(data)
            initLinksBlock(data)
                // setEventListner()

        })
        .fail(function() {
            $('#loader').hide();
            alert('Произошла ошибка при отправке данных!');
        })
    return false; // отменяем отправку данных формы
});

// function setEventListner() {
//     $('.elem').on('click', function() {
//         console.log(this)
//         let format = this.getAttribute('value')
//         ext = this.getAttribute('ext')
//         data = { format: format, url: VideoUrl }
//             // отправляем данные на сервер
//             // console.log(data)
//         $.ajax({
//                 url: url + 'site/link',
//                 type: action,
//                 data: data
//             })
//             .done(function(data) {
//                 // data = JSON.parse(data)
//                 // url = "https://r6---sn-xguxaxjvh-bvwe.googlevideo.com/videoplayback?expire=1628106217&ei=iZkKYb-XBZmp7QTT2ajQAw&ip=109.198.96.221&id=o-AErL2t1EHREw5R1rW32Pqq7I94_dbR3BlEFWKiwhHv7x&itag=18&source=youtube&requiressl=yes&mh=DS&mm=31%2C29&mn=sn-xguxaxjvh-bvwe%2Csn-n8v7znse&ms=au%2Crdu&mv=m&mvi=6&pcm2cms=yes&pl=19&initcwndbps=1800000&vprv=1&mime=video%2Fmp4&ns=jwV-8Cf4YRkZct1epQjLiyYG&gir=yes&clen=250881&ratebypass=yes&dur=14.767&lmt=1626515104865570&mt=1628084458&fvip=16&fexp=24001373%2C24007246&c=WEB&txp=5310222&n=bIn7qgGxcP7rofhrL&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cgir%2Cclen%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIgTkSJxuqf121VtmghKvZe07tOyDAAA79OJlb70VvYe08CIQC4_X9nMd-0o12ZRRvZ2vE8wd3nu6M46Hvp1VAKcZs0jw%3D%3D&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpcm2cms%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRAIgCUKXr5FypXiuEvQuUELJQj2XC-VTNfMM8ctQ7ZazRegCIHc6bU7-OwKi6WKUPPJesgpVa0DGZiGDz1RtQ3UufvHa"
//                 console.log(data)
//                 blob = new Blob([data], { type: 'video/' + ext });
//                 console.log(blob)
//                 const url = URL.createObjectURL(blob);
//                 const dummy = document.createElement('a');
//                 dummy.href = url;
//                 dummy.download = 'vid';

//                 document.body.appendChild(dummy);
//                 dummy.click(); // Готово
//             })

//         //         // А теперь маленький трюк, чтоб скачать
//         // const dummy = document.createElement('a');
//         // dummy.href = url;
//         // dummy.download = 'video';

//         // document.body.appendChild(dummy);
//         // dummy.click(); // Готово


//     })

// }


function initLinksBlock(data) {

    console.log($('#thumbnail'))
    $('#thumbnail')[0].src = data['thumbnail']
    elem = 0
    for (index in data["formats"]) {
        element = data["formats"][index]
        href = 'site/link?link=' + VideoUrl + '&format=' + index + '&ext=' + element[0]
        if (element[2]) {
            mute = '<i class="fas fa-volume-mute"></i>'
            el = '<a href=' + href + ' class="elem col-md-2"  value="' + index + '"> <p>' + element[1] + '</p> <p class="color">' + element[0] + '</p> <p class="color">' + mute + '</p> </a > '
        } else {
            el = '<a href=' + href + ' class="elem col-md-2" value="' + index + '"> <p>' + element[1] + '</p> <p class="color">' + element[0] + '</p> </a > '
        }
        $('#elem-container').append(el)
    }
}