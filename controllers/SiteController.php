<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\YouTubeDownloader;
use yii\components\Downloader;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function file_get_content_curl($url)
    {
        // Throw Error if the curl function does'nt exist.
        if (!function_exists('curl_init')) {
            die('CURL is not installed!');
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new YouTubeDownloader();

        $load = Yii::$app->request->post();
        // return $load;
        if (Yii::$app->request->isAjax) {
            if ($model->load($load) and $model->validate()) {

                $downloader = $model->getDownloader();
                // echo '<pre>'; print_r(json_encode($downloader->videoInfo, JSON_UNESCAPED_UNICODE)); echo '</pre>';
                return json_encode($downloader->videoInfo, JSON_UNESCAPED_UNICODE);
            } else {
                return json_encode($model->getErrors());
            }
        } else {

            return $this->render('index', ['model' => $model]);
        }
    }

    public function actionLink($link, $format, $ext)
    {

        $downloader = Yii::$container->get('app\components\Downloader', [], ['url' => $link, 'format' => $format]);

        $url = $downloader->download();
        $fileContent = $this->file_get_content_curl($url); // write image content to filename
        $name = $downloader->name . "." . $ext;
        
        $this->response->sendContentAsFile($fileContent, $name);
        return;
    }
}
