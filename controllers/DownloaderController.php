<?php

declare(strict_types=1);

namespace app\controllers;

use Yii;
use  yii\web\Request;
use  yii\web\Response;
use yii\web\Controller;
use app\models\YouTubeDownloader;
use YoutubeDl\Options;
use YoutubeDl\YoutubeDl;

class DownloaderController extends Controller
{

    public $modelClass = 'app\models\Project';
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::class,
                'formatParam' => '_format',
                // 'formats' => [
                //     'application/json' => \yii\web\Response::FORMAT_JSON,
                //     'xml' => \yii\web\Response::FORMAT_XML
                // ],
            ],
        ];
    }
    public function actionVideo()
    {
        
        $yt = new YoutubeDl();
        $yt->onProgress(function () {});
        $yt->setBinPath(Yii::getAlias('@webroot').'/youtube-dl');
        $collection = $yt->download(
            Options::create()
                ->downloadPath(Yii::getAlias('@video'))
                ->url('https://www.youtube.com/watch?v=oDAw7vW7H0c')
        );
        return $this->render("debug", ["yt"=>$yt,"collection" => $collection]);
    }

}
