<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use YoutubeDl;
use YoutubeDl\Options;
use YoutubeDl\YoutubeDl as YoutubeDlYoutubeDl;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class YTController extends Controller
{

    public function actionIndex()
    {
        $yt = new YoutubeDlYoutubeDl();
        $yt->onProgress(function () {});
        $yt->setBinPath(Yii::getAlias('@video').'/youtube-dl.exe');
        $url = 'https://www.youtube.com/watch?v=OBF4kZS9baw&ab_channel=FILINOJ';
        $video_id = $yt->getDownloadLinks(implode($url));
    }
}