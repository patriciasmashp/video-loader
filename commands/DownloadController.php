<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;


use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DownloadController extends Controller
{
    const PathToBin =  '@YTDL';

    // public function options($actionID)
    // {
        
    // }
    
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    private function YTDL(String $url, String $params = '')
    {
        
        $resoult = shell_exec(Yii::getAlias($this::PathToBin).' '.$params.' '.$url);
      
        // $filename = basename($url); // get only the filename
        
        // $fileContent = $this->file_get_content_curl($url); // get image content
        // file_put_contents(Yii::getAlias('@video/pic.jpg'), $fileContent); // write image content to filename
        return $resoult;
    }
    public function file_get_content_curl($url)
    {
        // Throw Error if the curl function does'nt exist.
        if (!function_exists('curl_init')) {
            die('CURL is not installed!');
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function actionIndex($url)
    {
        // $u = "https://r1---sn-xguxaxjvh-bvwe.googlevideo.com/videoplayback?expire=1628104266&ei=6pEKYa7bMdeMv_IP7J2GwA8&ip=109.198.96.221&id=o-AD2M6C_0N6tKRhpPCho7G3YGa-ABOnldAgOMlJyzEFXc&itag=18&source=youtube&requiressl=yes&mh=AG&mm=31%2C29&mn=sn-xguxaxjvh-bvwe%2Csn-n8v7znlr&ms=au%2Crdu&mv=m&mvi=1&pcm2cms=yes&pl=19&initcwndbps=1555000&vprv=1&mime=video%2Fmp4&ns=qv0UFntvtCmnS8IktaFFQhQG&gir=yes&clen=111663648&ratebypass=yes&dur=6118.643&lmt=1624012080229661&mt=1628082300&fvip=18&fexp=24001373%2C24007246&beids=9466587&c=WEB&txp=5530432&n=VY3Zfb6aYjmQLrdOG&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cgir%2Cclen%2Cratebypass%2Cdur%2Clmt&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpcm2cms%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRgIhAJKKyzHUUJ1xPNq5COpkVSJ_vRg-LSd9-pd_b2Xz897XAiEA0PiSSdoruZTqGbILr78u80RFZcNQPegegniZEFCzFog%3D&sig=AOq0QJ8wRAIgQbEduB4nzskRP_T_LD1FZoK0nD2j5Y6MOccWiYwKrnwCIDqfsXIMKT10zit1qpVa5jk58fBCuB6KSqsQhn74v5Ng";
        $u = "https://www.youtube.com/watch?v=admcvvTMRtU";
        $downloader = Yii::$container->get('app\components\Downloader',[],['url'=>$u,'format'=>'18']);
        echo '<pre>'; print_r($downloader->name); echo '</pre>'; 
        
        return ExitCode::OK;
    }

    private function structFromats(String $output){
        $rowDataArray = explode("\n",$output);
        $dataArray = [];
        foreach ($rowDataArray as $key => $value) {
            if (strstr($value,'          ') and !strstr($value,'audio only')) {
                $arValue = explode("       ",$value);
                $code = str_replace(" ","",$arValue[0]);
                $format = str_replace(" ","",$arValue[1]);;
                
                preg_match('/\d*p/m',$arValue[2],$resolution);
                $dataArray[$code] = [$format,$resolution[0]];
                
            }
        }
        return $dataArray;
    }
    
    public function actionFormats($url){
        return $this->structFromats($this->YTDL($url));

    }
}
