<?php

namespace app\components;

use yii\base\BaseObject;
use Yii;

class Downloader extends BaseObject
{

    const PathToBin =  '@YTDL';

    public $url;

    public $_formats;

    public $_videoFormat;

    public $_thumbnail;

    public $_name;

    public function __construct($config = [])
    {
        $this->url = $config['url'];
        if (!isset($config['format'])) {
            $this->_videoFormat = $config['format'];
        }

        parent::__construct($config);
    }

    public function file_get_content_curl($url)
    {
        // Throw Error if the curl function does'nt exist.
        if (!function_exists('curl_init')) {
            die('CURL is not installed!');
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    private function YTDL(String $url, String $params = '')
    {

        $resoult = shell_exec(Yii::getAlias($this::PathToBin) . ' ' . $params . ' ' . $url);
        // return Yii::getAlias($this::PathToBin) . ' ' . $params . ' ' . $url;
        // $filename = basename($url); // get only the filename

        // $fileContent = $this->file_get_content_curl($url); // get image content
        // file_put_contents(Yii::getAlias('@video/pic.jpg'), $fileContent); // write image content to filename
        return $resoult;
    }


    private function structFromats(String $output)
    {
        $rowDataArray = explode("\n", $output);
        $dataArray = [];
        // return $rowDataArray;
        foreach ($rowDataArray as $key => $value) {
            if (strstr($value, '          ') and !strstr($value, 'audio only')) {
                $arValue = explode("       ", $value);
                $code = str_replace(" ", "", $arValue[0]);
                $format = str_replace(" ", "", $arValue[1]);;

                preg_match('/\d*p/m', $arValue[2], $resolution);
                if(strstr($value, 'video only')){

                    $dataArray[$code] = [$format, $resolution[0],1];
                }
                else{

                    $dataArray[$code] = [$format, $resolution[0],0];
                }
            }
        }
        return $dataArray;
    }

    public function getFormats()
    {
        if (!isset($this->_formats)) {
            $this->_formats = $this->structFromats($this->YTDL($this->url, '-F'));
            return $this->_formats;
        }
        return $this->_formats;
    }

    public function setFormat($key)
    {
        $this->_videoFormat = $key;
    }

    public function getImg()
    {
        if (!$this->_thumbnail) {
            $output = $this->YTDL($this->url, '--list-thumbnails');
            $output = array_reverse(explode("\n", $output))[1];
            $link = array_pop(explode(' ', $output));
            $this->_thumbnail = $link;
        }
        return $this->_thumbnail;
    }

    public function getName()
    {
        if (!$this->name) {
            $this->_name = $this->YTDL($this->url, '-e');
        }
        return str_replace("\n", '',$this->_name);
    }

    public function getVideoInfo()
    {
        $info = [
            
            "thumbnail" => $this->getImg(),
            "formats" => $this->getFormats(),
            
        ];
        // $output = $this->YTDL($this->url, '-j');
        return  $info;
    }
    public function download()
    {
        $link = $this->YTDL($this->url, '-f '.$this->_videoFormat.' -g');
        // echo '<pre>'; print_r($link); echo '</pre>';
        return str_replace("\n", '',$link);
    }
   
}
