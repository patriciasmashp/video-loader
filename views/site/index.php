<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Video Downloader';
?>
<div class="row" style="width: 100%;">
    <div class="col-md-3">
        <h1 class="col-md-2">Video Loader</h1>
    </div>
    <div class="col-md-9" style="
            align-items: center;
            display: flex;
        ">


        <? $form = ActiveForm::begin([
            'id' => 'form-url-input',
            'options' => [
                "style" => "align-items: center; display: flex;",
                'class' => 'col-md-9',
                'enctype' => 'multipart/form-data'
            ],
            'fieldConfig' => [
                'template' => "\n{input}\n",
            ],
        ]); ?>
        <div class="input-container row">
            <?= $form->field($model, 'video_url', ['options' => ['class' => 'col-md-11'],])->textInput(["placeholder" => "Video URl", "class" => "col-md-10 url-input"])->hint(false)->label(false); ?>
            <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-w-svg col-md-1 ']); ?>

        </div>

        <?
        
        ActiveForm::end();

        
        ?>

    </div>
</div>
<div style="display: flex; justify-content: center;" id="links-block">
    <div id="loader">
        <i class="fas fa-circle-notch"></i>
    </div>
    <div class="row" >
        <div class="col-md-4" id="info-cont">
            <img class="img-fluid" src="" alt="" id="thumbnail">
        </div>
        <div class="row col-md-8" style="align-items: center; flex-wrap: wrap;" id="elem-container">

        </div>

    </div>
</div>