<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\components\Downloader;
use YoutubeDl\Options;
use YoutubeDl\YoutubeDl;

/** 
 * CodexWorld 
 * 
 * This Downloader class helps to download youtube video. 
 * 
 * @class       YouTubeDownloader 
 * @author      CodexWorld 
 * @link        http://www.codexworld.com 
 * @license     http://www.codexworld.com/license 
 */
class YouTubeDownloader extends Model
{
    /* 
     * Video Id for the given url 
     */
    private $videoId;


    /* 
     * Video information for the given vide 
     */
    private $_videoInfo;


    /* 
     * Video title for the given video 
     */
    private $videoTitle;

    /* 
     * Full URL of the video 
     */
    public $video_url;

    public function rules()
    {
        $pattern = '/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed)\/))([^\?&\"\'>]+)/';
        return [
            // username and password are both required
            [['video_url'], 'required'],
            // rememberMe must be a boolean value
            ['video_url', 'string'],
            // password is validated by validatePassword()
            // ['video_url', 'match', 'pattern' => $pattern],
            ['video_url', 'validateUrl'],
            ['videoInfo', 'validateVideo']

        ];
    }

    public function validateUrl($attribute, $params)
    {

        $pattern = '/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed)\/))([^\?&\"\'>]+)/';
        if (!$this->hasErrors()) {
            $url = $this->video_url;

            if (!$url || !preg_match($pattern, $url)) {
                $this->addError($attribute, 'Incorrect url.');
            }
        }
    }
    public function validateVideo($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $info = $this->getVideoInfo();
            if ($info["items"][0]["snippet"]["liveBroadcastContent"] == "live") {
                $this->addError($attribute, 'cant download stream');
            }
        }
    }

    private function file_get_contents_curl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    /* 
     * Get the video information 
     * return string 
     */
    
    public function getVideoInfo()
    {
        if (!$this->_videoInfo) {
            $url = "https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=" . $this->extractVideoId($this->video_url) . "&key=AIzaSyB5v8fKR5XpAOeR34B0dEbxLbmIgG1QQuI";
            $info = json_decode($this->file_get_contents_curl($url), true);
            $this->_videoInfo = $info;
            return $this->_videoInfo;
        }
        return $this->_videoInfo;
    }
    /* 
     * Get video Id 
     * @param string 
     * return string 
     */
    private function extractVideoId($video_url)
    {
        //parse the url 
        $parsed_url = parse_url($video_url);
        if ($parsed_url["path"] == "youtube.com/watch") {
            $this->video_url = "https://www." . $video_url;
        } elseif ($parsed_url["path"] == "www.youtube.com/watch") {
            $this->video_url = "https://" . $video_url;
        }

        if (isset($parsed_url["query"])) {
            $query_string = $parsed_url["query"];
            //parse the string separated by '&' to array 
            parse_str($query_string, $query_arr);
            if (isset($query_arr["v"])) {
                return $query_arr["v"];
            }
        }
    }

    /* 
     * Get the downloader object if pattern matches else return false 
     * @param string 
     * return object 
     *  
     */
    public function getDownloader()
    {
        $downloader = Yii::$container->get('app\components\Downloader',[],['url'=>$this->video_url]);
        return $downloader;
    }

    /* 
     * Get the video download link 
     * return array 
     */
    public function getVideo()
    {
        $info = $this->getVideoInfo();

        $yt = $this->getDownloader();
        $collection = $yt->download(
            Options::create()
                ->downloadPath(Yii::getAlias('@video'))
                ->url($this->video_url)
        );

        return $collection;
    }

    /* 
     * Validate the given video url 
     * return bool 
     */
    public function hasVideo()
    {
        $valid = true;

        parse_str($this->getVideoInfo(), $data);

        if ($data["status"] == "fail") {
            $valid = false;
        }

        return $valid;
    }
}
